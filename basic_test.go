package errs

import (
	"fmt"
	"io"
	"testing"
	"time"
)

var TestExampleErr = New("an error is formed on line ten during initialization")

func TestOne(t *testing.T) {

	_, err := ex()
	if err != nil {
		//fmt.Println(err)
		//fmt.Println(err.Error())
		fmt.Println(GetString(err))
	}
	_, err2 := ex2(t)
	if err != nil {
		//fmt.Println(err2)
		//fmt.Println(err2.Error())
		fmt.Println(GetString(err2))
	}
	e := longProcess()
	if e == nil {
		t.Error("expected error, got nil")
		return
	}

	if !IsErr(e) {
		t.Error("expected Err, got NotErr / normal error")
		return
	}

	if e == TestExampleErr {
		t.Error("is eq")
	}

	e1, ok := GetErr(e)
	if !ok {
		t.Error("not ok")
	}
	if e1.E() != TestExampleErr.E() {
		t.Error("not eq", e1.E(), TestExampleErr.E())
		return
	}

	fmt.Println(e1.String())
}

func TestWrap(t *testing.T) {
	_, err := ex()
	e := Wrap(err)
	fmt.Println(e.String())
}

func ex() (io.Writer, error) {
	err := Errorf("broken stack @ %s", time.Now().UTC().Format(time.Kitchen))
	return nil, err
}

func longProcess() error {
	return Wrap(TestExampleErr)
}

func ex2(t *testing.T) (io.Writer, error) {
	err := New("broken example 2")
	return nil, Wrap(err)
}
