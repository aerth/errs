package errs

import (
	"context"
	"fmt"
	"runtime"
	"time"
)

type err struct {
	Text      string
	ctx       context.Context
	numFrames int
	frames    *runtime.Frames
	caller    runtime.Frame
	time      time.Time
	e         error // underlying basic error
}

var (
	MaxCallers = 15
	SkipFrame  = 2
)

func IsNil(e error) bool {
	return e == nil
}

func IsNotNil(e error) bool {
	return e != nil
}

func IsErr(e error) bool {
	_, ok := e.(*err)
	return ok
}

// GetErr is a useful func
func GetErr(e error) (*err, bool) {
	e2, ok := e.(*err)
	return e2, ok
}

func Wrap(e error) *err {
	if e == nil {
		return nil
	}

	pc := make([]uintptr, MaxCallers)
	n := runtime.Callers(SkipFrame, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	e2, ok := GetErr(e)
	if ok {
		e = e2.E()
	}

	return &err{
		ctx:       context.TODO(),
		numFrames: n,
		frames:    frames,
		caller:    frame,
		time:      time.Now(),
		Text:      e.Error(),
		e:         e,
	}
} // copied code from Error()

type simpleErr string

func (s simpleErr) Error() string {
	return string(s)
}

func Errorf(f string, i ...interface{}) *err {
	text := fmt.Sprintf(f, i...)
	pc := make([]uintptr, MaxCallers)
	n := runtime.Callers(SkipFrame, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return &err{
		ctx:       context.TODO(),
		numFrames: n,
		frames:    frames,
		caller:    frame,
		time:      time.Now(),
		Text:      text,
		e:         simpleErr(text),
	}
}
func New(text string) *err {
	pc := make([]uintptr, MaxCallers)
	n := runtime.Callers(SkipFrame, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return &err{
		ctx:       context.TODO(),
		numFrames: n,
		frames:    frames,
		caller:    frame,
		time:      time.Now(),
		Text:      text,
		e:         simpleErr(text),
	}
}

// E is for comparison to known errors
func (e *err) E() error {
	return e.e
}

// String returns caller information
func (e err) String() string {
	frame := e.caller
	file := frame.File
	for i := len(frame.File) - 1; i > 0; i-- {
		if frame.File[i] == '/' {
			file = frame.File[i+1:]
			break
		}
	}
	shortFn := frame.Function
	for i := len(frame.Function) - 1; i > 0; i-- {
		if frame.Function[i] == '/' {
			shortFn = frame.Function[i+1:]
			break
		}
	}
	return fmt.Sprintf("%s:%d %s(%q)", file, frame.Line, shortFn, e.Text)
}

// Error is normal (but not immutable) error
func (e err) Error() string {
	return e.Text
}

func GetString(e error) string {
	if e == nil {
		return "nil error"
	}
	switch e.(type) {
	case *err:
		return e.(*err).String()
	case err:
		return e.(err).String()
	default:
		return e.Error()
	}
}
